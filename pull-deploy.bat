@echo on

git clean -f || goto :errorGit
git reset --hard || goto :errorGit
git reset --merge || goto :errorGit

git fetch --all || goto :errorGit

git merge -X theirs origin master  || goto :errorGit

call deploy.bat

goto :exit

:errorGit
echo "git failed"
pause
goto exit

:exit
pause