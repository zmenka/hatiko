@echo off

:: BatchGotAdmin
:-------------------------------------
REM  --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
	REM Reset error level
	set errorlevel=0
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"
	if '%errorlevel%' NEQ '0' (
		del "%temp%\getadmin.vbs"
		goto errorAdmin
	)
    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    goto exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
:--------------------------------------

SC stop hatiko

SET INSTALL_UTL="%SYSTEMROOT%\Microsoft.NET\Framework\v4.0.30319"

IF exist %INSTALL_UTL% (
%INSTALL_UTL%\InstallUtil.exe /u hatiko\hatiko\hatiko\bin\Release\hatiko.exe

%INSTALL_UTL%\MSBuild.exe "hatiko\hatiko.sln" /p:Configuration=Release

%INSTALL_UTL%\InstallUtil.exe hatiko\hatiko\hatiko\bin\Release\hatiko.exe
if errorlevel 1 goto errorInstall

SC start hatiko
REM start http://localhost:8087/

goto exit
) else (
goto errorPath
)


:errorAdmin
echo Failed to request admin rights
pause
goto exit

:errorInstall
echo errorStop
pause
goto exit

:errorPath
echo %INSTALL_UTL% not found!
pause
goto exit


:exit
pause