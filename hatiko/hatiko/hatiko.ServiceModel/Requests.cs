﻿using System;
using ServiceStack;
using System.Linq;

namespace hatiko.ServiceModel
{
    [Route("/hello/{Name}")]
    public class Hello : IReturn<HelloResponse>
    {
        public string Name { get; set; }
    }

    [Route("/secret")]
    public class BasicEncryptedMessage : IReturn<BasicEncryptedMessageResponse>
    {
        public string OperationName { get; set; }
        public string EncryptedBody { get; set; }
    }

    [Route("/do", "GET")]
    public class TaskRequest : IReturn<GeneralResponse>
    {
        public string uid { get; set; }
        public string write { get; set; }
        public string delay { get; set; }
        public string n { get; set; }
        public string nafter { get; set; }

        public override string ToString()
        {
            return "uid:" + uid + ", write:" + write + ", delay:" + delay + ", n:" + n + ", nafter: " + nafter;
        }
    }

    [Route("/domany", "GET")]
    public class TaskManyRequest : IReturn<GeneralResponse>
    {
        public TaskRequest[] tasks { get; set; }

        public override string ToString()
        {
            return string.Join(";", tasks.Select(w => w.ToString()));
        }
    }

    [Route("/update_config", "GET")]
    public class UpdateConfig : IReturn<GeneralResponse>
    {
    }
    //[Route("/do", "POST")]
    //public class TasksRequest : IReturn<GeneralResponse>
    //{
    //    public TaskRequest[] tasks { get; set; }

    //    public override string ToString()
    //    {
    //        return string.Join(";", tasks.Select(w => w.ToString()));
    //    }
    //}
}
