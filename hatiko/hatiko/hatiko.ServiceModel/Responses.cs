﻿using System;
using ServiceStack;

namespace hatiko.ServiceModel
{
    public class HelloResponse
    {
        public string Result { get; set; }
    }

    public class BasicEncryptedMessageResponse
    {
        public string OperationName { get; set; }
        public string EncryptedBody { get; set; }

        public ResponseStatus ResponseStatus { get; set; }
    }

    public class GeneralResponse
    {
        public bool Success { get; set; }
        public object Data { get; set; }
    }
}
