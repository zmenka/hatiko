﻿using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;
using hatiko.ServiceInterface;

namespace hatiko
{
    public class TaskConfig
    {
        private IList<string> config;
        private string uid;

        public TaskConfig(string uid)
        {
            this.uid = uid;
            //var appSettings = new TextFileSettings("~/../task.csv".MapHostAbsolutePath(), ",");
            var appSettings = ConfigSingleton.Instance;
            this.config = appSettings.getListByUid(uid);
            if (this.config.Count != 6)
            {
                throw new Exception(string.Format("Task with uid {0} has wrong config. Go to admin. ", this.uid));
            }
        }

        public string DeviceType
        {            //get the person name 
            get { return DeviceTypeEnum.Props[this.config[0]]; }
        }

        public IFunc TypeFunc
        {            //get the person name 
            get { return TypeFuncEnum.Props[this.config[3]]; }
        }

        public string DeviceAddress
        {            //get the person name 
            get { return this.config[1]; }
        }

        public string Source
        {            //get the person name 
            get { return this.config[2]; }
        }

        public string AsyncInfo
        {            //get the person name 
            get { return this.config[5]; }
        }
    }

    public class DeviceTypeEnum
    {
        public static readonly Dictionary<string, string> Props = new Dictionary<string, string>()
            {
                { "MB", "MB"},
                {"NIAnalog","NIAnalog"},
                {"NIDigital","NIDigital"},
                {"Virtual","Virtual"}
            };

        public static string Modbus { get { return Props["MB"]; } }
        public static string NIAnalog { get { return Props["NIAnalog"]; } }
        public static string NIDigital { get { return Props["NIDigital"]; } }
        public static string Virtual { get { return Props["Virtual"]; } }
    }

    public class TypeFuncEnum
    {
        public static readonly Dictionary<string, IFunc> Props = new Dictionary<string, IFunc>()
            {
                { "floatMB", Converters.RegistersToFloat},
                { "bool1000", Converters.RegistersToBool1000},
                { "int32MB", Converters.RegistersToInt32},
                { "int16MB", Converters.RegistersToInt16},
                { "VoltsNI", Converters.Volts},
                { "VoltsToAmpNI", Converters.VoltsToAmp},
                { "boolNI", Converters.BoolNI},
                { "uintNI", Converters.UintNI},
                { "MB_table_bool_4", Converters.MB_table_bool_4},
                { "MB_many_read", Converters.MB_many_read},
                { "MB_bool_2", Converters.MB_bool_2},
                { "PHV", Converters.PHV},
                { "intSatecMB", Converters.intSatecMB}
            };
    }

    public class ValueTypeEnum
    {
        public static readonly Dictionary<string, Type> Props = new Dictionary<string, Type>()
            {
                { "float", typeof(float)},
                {"bool",typeof(bool)}
            };
    }


}
