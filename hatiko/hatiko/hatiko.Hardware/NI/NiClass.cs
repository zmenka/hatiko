﻿using System;
using System.Collections.Generic;
using NationalInstruments.DAQmx;

namespace hatiko.NI
{
    public class NiClass
    {
        public void Do()
        {
            try
            {
                var chanels = DaqSystem.Local.GetPhysicalChannels(PhysicalChannelTypes.DIPort, PhysicalChannelAccess.External);

                using (Task digitalReadTask = new Task())
                {
                    digitalReadTask.DIChannels.CreateChannel(
                        "Dev1/port0",
                        "port0",
                        ChannelLineGrouping.OneChannelForAllLines);

                    DigitalSingleChannelReader reader = new DigitalSingleChannelReader(digitalReadTask.Stream);
                    UInt32 data = reader.ReadSingleSamplePortUInt32();

                   
                }
            }
            catch (DaqException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

        }
    }
}
