﻿using System;
using System.Collections.Generic;
using NationalInstruments.DAQmx;
using System.Data;
using NationalInstruments;

namespace hatiko.NI.Digital
{
    public class ReadChannel
    {
        private Task myTask;
        private DigitalSingleChannelReader myDigitalReader;

        public object Start(string physicalChannel)
        {
            string[] a = physicalChannel.Split('/');
            if (a.Length != 2 && a.Length != 3)
            {
                throw new Exception("not correct format of DeviceAddress " + physicalChannel);
            }

            try
            {
                //Create a task such that it will be disposed after
                //we are done using it.
                myTask = new Task();

                //Create channel
                myTask.DIChannels.CreateChannel(physicalChannel, "",
                    ChannelLineGrouping.OneChannelForAllLines);

                myDigitalReader = new DigitalSingleChannelReader(myTask.Stream);

                if (a.Length == 2)
                {
                    return myDigitalReader.ReadSingleSamplePortUInt32();
                }
                else
                {
                    return myDigitalReader.ReadSingleSampleSingleLine();
                }
            }
            catch (DaqException exception)
            {
                Console.WriteLine(exception.Message);
                throw exception;
            }
            finally
            {
                myTask.Dispose();
            }
        }

    }
}
