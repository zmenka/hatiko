﻿using System;
using System.Collections.Generic;
using NationalInstruments.DAQmx;
using System.Data;
using NationalInstruments;
using System.Linq;
using System.Collections;


namespace hatiko.NI.Digital
{
    public class WriteChannel
    {
        private Task myTask;
        private DigitalSingleChannelWriter writer;

        public void Start(string DeviceAddress, object valueInput)
        {
            string[] a = DeviceAddress.Split('/');
            if (a.Length != 2 && a.Length != 3)
            {
                throw new Exception("not correct format of DeviceAddress " + DeviceAddress);
            }

            try
            {
                //Create a task such that it will be disposed after
                //we are done using it.
                myTask = new Task();

                myTask.DOChannels.CreateChannel(DeviceAddress, "",
                         ChannelLineGrouping.OneChannelForAllLines);
                //BitArray b = new BitArray(new byte[] { value });
                //bool[] bits = b.Cast<bool>().ToArray();
                writer = new DigitalSingleChannelWriter(myTask.Stream);

                if (a.Length == 2){
                    writer.WriteSingleSamplePort(true, (UInt32)valueInput);
                }
                else {
                    writer.WriteSingleSampleSingleLine(true, (bool)valueInput);
                }
                
            }
            catch (DaqException exception)
            {
                Console.WriteLine(exception.Message);
                throw exception;
            }
            finally
            {
                myTask.Dispose();
            }
        }

    }
}
