﻿using System;
using System.Collections.Generic;
using NationalInstruments.DAQmx;
using System.Data;
using NationalInstruments;

namespace hatiko.NI.AnalogIn
{
    public class VoltageRange
    {
        private Task myTask;
        private AnalogMultiChannelReader reader;
        private AnalogWaveform<double>[] data;
        private int channelIndex = 0;

        public double[] Start(string deviceAddress, string source, int limit)
        {
            string[] a = source.Split(':');
            if (a.Length != 3)
            {
                throw new Exception("not correct format of source " + source);
            }

            double rangeMinimum;
            if (!Double.TryParse(a[0], out rangeMinimum))
            {
                throw new Exception("not correct rangeMinimum " + a[0]);
            }

            double rangeMaximum;
            if (!Double.TryParse(a[1], out rangeMaximum))
            {
                throw new Exception("not correct rangeMaximum " + a[1]);
            }

            double rate;
            if (!Double.TryParse(a[2], out rate))
            {
                throw new Exception("not correct rate " + a[2]);
            }

            try
            {
                // Create a new task
                myTask = new Task();

                // Create a channel
                myTask.AIChannels.CreateVoltageChannel(deviceAddress, "",
                    (AITerminalConfiguration)(-1), rangeMinimum, rangeMaximum, AIVoltageUnits.Volts);
                channelIndex = myTask.AIChannels.Count - 1;
                // Configure timing specs    
                myTask.Timing.ConfigureSampleClock("", rate, SampleClockActiveEdge.Rising,
                    SampleQuantityMode.FiniteSamples, limit);

                // Verify the task
                myTask.Control(TaskAction.Verify);

                // Read the data
                reader = new AnalogMultiChannelReader(myTask.Stream);

                data = reader.ReadWaveform(limit);

                return getResult(data);

            }
            catch (DaqException exception)
            {
                Console.WriteLine(exception.Message);
                throw exception;
            }
            finally
            {
                myTask.Dispose();
            }
        }

        private double[] getResult(AnalogWaveform<double>[] sourceArray)
        {
            if (sourceArray == null || sourceArray.Length != 1)
            {
                throw new DaqException("not correct data was recieved");
            }
            double[] res = new double[sourceArray[channelIndex].Samples.Count];

            for (int sample = 0; sample < sourceArray[channelIndex].Samples.Count; ++sample)
            {
                res[sample] = sourceArray[channelIndex].Samples[sample].Value;
            }
            return res;
        }

    }
}
