﻿using System;
using System.Collections.Generic;
using NationalInstruments.DAQmx;
using System.Data;
using NationalInstruments;
using System.Threading;

namespace hatiko.NI.AnalogIn
{

    public class VoltageRangeWithTrigger
    {
        private Task myTask;
        private AnalogMultiChannelReader reader;
        private double[,] pretrigger;

        private DataColumn[] dataColumn = null;
        private DataTable dataTable = new DataTable();
        private int limit;
        private int limitAfter;
        private int limitDownload;
        private double level = 0;
        private bool needAboveLevel = false;
        private int channelIndex = 0;
        private int ptSize = 0;

        public object[] Start(string deviceAddress, string source, int limit, int limitAfter, string asyncInfo)
        {
            this.limit = limit;

            if (limitAfter > 0)
            {
                this.limitAfter = limitAfter;
            }
            else
            {
                this.limitAfter = limit;
            }

            limitDownload = limitAfter < limit ? limitAfter : limit;

            string[] a = source.Split(':');
            if (a.Length != 3)
            {
                throw new Exception("not correct format of source " + source);
            }

            double rangeMinimum;
            if (!Double.TryParse(a[0], out rangeMinimum))
            {
                throw new Exception("not correct rangeMinimum " + a[0]);
            }

            double rangeMaximum;
            if (!Double.TryParse(a[1], out rangeMaximum))
            {
                throw new Exception("not correct rangeMaximum " + a[1]);
            }

            double rate;
            if (!Double.TryParse(a[2], out rate))
            {
                throw new Exception("not correct level " + a[2]);
            }

            string[] b = asyncInfo.Split(':');
            if (b.Length != 2)
            {
                throw new Exception("not correct format of asyncInfo " + asyncInfo);
            }

            if (!Double.TryParse(b[0], out this.level))
            {
                throw new Exception("not correct level " + b[0]);
            }

            if (!Boolean.TryParse(b[1], out this.needAboveLevel))
            {
                throw new Exception("not correct needAboveLevel " + b[1]);
            }

            try
            {
                // Create the task
                myTask = new Task();

                // Create channels
                myTask.AIChannels.CreateVoltageChannel(deviceAddress,
                    "",
                    (AITerminalConfiguration)(-1),
                    rangeMinimum,
                    rangeMaximum,
                    AIVoltageUnits.Volts);

                this.channelIndex = myTask.AIChannels.Count - 1;
                // Set up timing
                myTask.Timing.ConfigureSampleClock("",
                    rate,
                    SampleClockActiveEdge.Rising,
                    SampleQuantityMode.ContinuousSamples, 1000);

                // Verify the task
                myTask.Control(TaskAction.Verify);

                // Prepare pretrigger samples
                pretrigger = new double[myTask.AIChannels.Count, limit];
                ptSize = limit;

                // Prepare the table for data
                InitializeDataTable();

                reader = new AnalogMultiChannelReader(myTask.Stream);

                return processData();

            }
            catch (DaqException exception)
            {
                Console.WriteLine(exception.Message);
                throw exception;
            }
            finally
            {
                myTask.Dispose();
            }
        }

        private object[] processData()
        {
            var task = System.Threading.Tasks.Task.Run(() => {
                while (ReadData())
                {

                }
            });
            task.Wait(5000);

            return getResult();
        }


        private bool ReadData()
        {
                // Read the data
                    double[,] data = reader.ReadMultiSample(limitDownload);

                    // Analyze the data for a start trigger
                    int triggerLocation = FindTrigger(data);

                    // Read the next set of data
                    if (triggerLocation != -1)
                    {
                        // Found a trigger
                        int iDisplay = 0;

                        // Display pretrigger samples
      
                        // Figure out how many samples we need from the pretrigger buffer
                        int deficit = limit - triggerLocation;

                        // Display samples from pretrigger buffer
                        for (int iData = 0; iData < deficit; iData++)
                        {
                            for (int iChan = 0; iChan < myTask.AIChannels.Count; iChan++)
                            {
                                dataTable.Rows[iDisplay][iChan] = pretrigger[iChan, iData + ptSize - deficit];
                            }

                            iDisplay++;
                        }

                        // Now include all samples up to the trigger location in data
                        for (int iData = 0; iData < triggerLocation; iData++)
                        {
                            for (int iChan = 0; iChan < myTask.AIChannels.Count; iChan++)
                            {
                                dataTable.Rows[iDisplay][iChan] = data[iChan, iData];
                            }

                            iDisplay++;
                        }
                     
                        // Display data after the trigger
                        for (int iData = triggerLocation; iData < limitDownload; iData++)
                        {
                            for (int iChan = 0; iChan < myTask.AIChannels.Count; iChan++)
                            {
                                dataTable.Rows[iDisplay][iChan] = data[iChan, iData];
                            }
                            iDisplay++;
                        }

                        int deficitAfter = limitAfter - (limitDownload - triggerLocation);
                        // Read more data
                        data = reader.ReadMultiSample(deficitAfter);

                        // Display additional data after trigger
                        for (int iData = 0; iData < deficitAfter; iData++)
                        {
                            for (int iChan = 0; iChan < myTask.AIChannels.Count; iChan++)
                            {
                                dataTable.Rows[iDisplay][iChan] = data[iChan, iData];
                            }

                            iDisplay++;
                        }
                       

                        // Stop the task
                        myTask.Dispose();
                        Console.WriteLine("SUCCESS");
                        return false;
                    }
                    else // triggerLocation == -1
                    {
                        
                        // Save over multiple iterations
                        int offset = ptSize - limitDownload;

                        // Shift elements in the array (discarding the first samples of data)
                        for (int iChan = 0; iChan < myTask.AIChannels.Count; iChan++)
                        {
                            for (int iData = 0; iData < offset; iData++)
                            {
                                pretrigger[iChan, iData] = pretrigger[iChan, iData + limitDownload];
                            }
                        }

                        // Copy the new data into the array
                        for (int iChan = 0; iChan < myTask.AIChannels.Count; iChan++)
                        {
                            for (int iData = 0; iData < limitDownload; iData++)
                            {
                                pretrigger[iChan, iData + offset] = data[iChan, iData];
                            }
                        }
                        
                        
                        // Read the next set of samples
                        Console.WriteLine("Start");
                        return true;
                    }

        }

        private int FindTrigger(double[,] data)
        {
            int triggerLocation = -1;

            if (this.needAboveLevel)
            {
                // Trigger if we find a voltage above the level
                for (int i = 0; i < data.GetLength(1); i++)
                {
                    if (data[0,i] > level)
                    {
                        triggerLocation = i;
                        break;
                    }
                }
            }
            else
            {
                // Trigger if we find a voltage below the level
                for (int i = 0; i < data.GetLength(1); i++)
                {
                    if (data[0,i] < level)
                    {
                        triggerLocation = i;
                        break;
                    }
                }
            }

            return triggerLocation;
        }

        private void InitializeDataTable()
        {
            dataTable.Rows.Clear();
            dataTable.Columns.Clear();
            dataColumn = new DataColumn[myTask.AIChannels.Count];

            for (int iChan = 0; iChan < myTask.AIChannels.Count; iChan++)
            {
                dataColumn[iChan] = new DataColumn();
                dataColumn[iChan].DataType = typeof(double);
                dataColumn[iChan].ColumnName = myTask.AIChannels[iChan].PhysicalName;
            }

            dataTable.Columns.AddRange(dataColumn);

            for (int iData = 0; iData < limit + limitAfter; iData++)
            {
                object[] rowArr = new object[myTask.AIChannels.Count];
                dataTable.Rows.Add(rowArr);
            }

        }

        private object[] getResult()
        {
            var res = new object[dataTable.Rows.Count];
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                var row = dataTable.Rows[i];
                res[i] = row[channelIndex];
            }

            return res;
        }

    }
}
