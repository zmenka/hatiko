﻿using System;
using System.Collections.Generic;
using NationalInstruments.DAQmx;
using System.Data;
using NationalInstruments;

namespace hatiko.NI.AnalogIn
{
    public class VoltageOne
    {
        private Task myTask;
        private AnalogMultiChannelReader reader;
        private double[] data;

        public double Start(string deviceAddress, string source)
        {
            string[] a = source.Split(':');
            if (a.Length != 2)
            {
                throw new Exception("not correct format of source " + source);
            }

            double minimumValue;
            if (!Double.TryParse(a[0], out minimumValue))
            {
                throw new Exception("not correct minimumValue " + a[0]);
            }

            double maximumValue;
            if (!Double.TryParse(a[1], out maximumValue))
            {
                throw new Exception("not correct maximumValue " + a[1]);
            }

            try
            {
                // Create a new task
                myTask = new Task();

                // Create a channel
                myTask.AIChannels.CreateVoltageChannel(deviceAddress , "",
                    (AITerminalConfiguration)(-1), minimumValue, maximumValue, AIVoltageUnits.Volts);
                var channelIndex = myTask.AIChannels.Count - 1;
                // Verify the task
                myTask.Control(TaskAction.Verify);

                // Prepare the table for data
                //InitializeDataTable(myTask.AIChannels, ref dataTable);

                // Read the data
                reader = new AnalogMultiChannelReader(myTask.Stream);

                data = reader.ReadSingleSample();

                return data[channelIndex];
            }
            catch (DaqException exception)
            {
                Console.WriteLine(exception.Message);
                throw exception;
            }
            finally
            {
                myTask.Dispose();
            }
        }
    }
}
