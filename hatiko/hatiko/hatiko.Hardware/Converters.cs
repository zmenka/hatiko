﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace hatiko
{
    public static class Converters
    {
        public static object RegistersToBool1000(object inputValue, bool backward = false)
        {
            if (backward)
            {
                bool value;
                if (!Boolean.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in RegistersToFloat not correct bool value " + inputValue);
                }
                int newValue = value ? 1000 : 0;
                return Converters.RegistersToInt16(newValue, backward);
            }
            else
            {
                int res = (short)Converters.RegistersToInt16(inputValue, backward);
                return res > 0 ? true : false;
            }
        }

        public static object intSatecMB(object inputValue, bool backward = false)
        {
            if (backward)
            {
                return RegistersToInt16(inputValue, backward);
            }
            else
            {
                short value = (short)RegistersToInt16(inputValue, backward);
                return value * 100.0 / 9999.0;
            }
        }

        public static object RegistersToFloat(object inputValue, bool backward = false)
        {
            if (backward)
            {
                float value;
                if (!Single.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in RegistersToFloat not correct float value " + inputValue);
                }
                var bytes = BitConverter.GetBytes(value);
                var registers = new ushort[2];
                registers[0] = BitConverter.ToUInt16(bytes, 0);
                registers[1] = BitConverter.ToUInt16(bytes, 2);
                return registers;
            }
            else
            {
                ushort[] registers = (ushort[])inputValue;
                if (registers == null || registers.Length != 2)
                {
                    throw new Exception("not correct length of registers array");
                }
                byte[] bits = new byte[registers.Length * 2];
                int place = (registers.Length - 1) * 2;
                for (int i = 0; i < registers.Length; i++)
                {
                    var r = BitConverter.GetBytes(registers[i]);
                    BitConverter.GetBytes(registers[i]).CopyTo(bits, place);
                    place -= 2;
                }

                return BitConverter.ToSingle(bits, 0);
            }            
        }

        public static object RegistersToInt32(object inputValue, bool backward = false)
        {
            if (backward)
            {
                int value;
                if (!Int32.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in RegistersToInt32 not correct int value " + inputValue);
                }
                var bytes = BitConverter.GetBytes(value);
                var registers = new ushort[2];
                registers[0] = BitConverter.ToUInt16(bytes, 0);
                registers[1] = BitConverter.ToUInt16(bytes, 2);
                return registers;
            }
            else
            {
                ushort[] registers = (ushort[])inputValue;
                if (registers == null || registers.Length != 2)
                {
                    throw new Exception("not correct length of registers array");
                }
                byte[] bits = new byte[registers.Length * 2];
                int place = (registers.Length - 1) * 2;
                for (int i = 0; i < registers.Length; i++)
                {
                    var r = BitConverter.GetBytes(registers[i]);
                    BitConverter.GetBytes(registers[i]).CopyTo(bits, place);
                    place -= 2;
                }

                return BitConverter.ToInt32(bits, 0);
            }
        }

        public static object RegistersToInt16(object inputValue, bool backward = false)
        {
            if (backward)
            {
                short value;
                if (!short.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in RegistersToInt16 not correct short value " + inputValue);
                }
                var bytes = BitConverter.GetBytes(value);
                var registers = new ushort[1];
                registers[0] = BitConverter.ToUInt16(bytes, 0);
                return registers;
            }
            else
            {
                ushort[] registers = (ushort[])inputValue;
                if (registers == null || registers.Length != 1)
                {
                    throw new Exception("not correct length of registers array");
                }
                byte[] bits = new byte[registers.Length * 2];
                int place = (registers.Length - 1) * 2;
                for (int i = 0; i < registers.Length; i++)
                {
                    var r = BitConverter.GetBytes(registers[i]);
                    BitConverter.GetBytes(registers[i]).CopyTo(bits, place);
                    place -= 2;
                }

                return BitConverter.ToInt16(bits, 0);
            }
        }

        public static object VoltsToAmp(object inputValue, bool backward = false)
        {
            float value;
            if (!Single.TryParse(inputValue.ToString(), out value))
            {
                throw new Exception("in VoltsToAmp not correct float value " + inputValue);
            }
            if (backward)
            {
                return value/10.0;
            }
            else
            {
                return value * 10.0;
            }
        }

        public static object MB_table_bool(object inputValue, bool backward = false)
        {
            if (backward)
            {
                var value = String.Join("", ((List<object>)inputValue).Select(x => (bool)x ? "1" : "0"));
                return Convert.ToInt32(value, 2);
            }
            else
            {
                int value;
                if (!Int32.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in MB_table_bool not correct short value " + inputValue);
                }

                if ( value< 0)
                {
                    throw new Exception("in MB_table_bool not correct short value " + inputValue + ", need in range [0,...]");
                }
                return Convert.ToString(value, 2);
            }
        }

        public static object MB_table_bool_4(object inputValue, bool backward = false)
        {
            if (backward)
            {
                return MB_table_bool(inputValue, backward);
            }
            else
            {
                int value;
                if (!Int32.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in MB_table_bool_4 not correct short value " + inputValue);
                }

                if (value < 0 || value > 15)
                {
                    throw new Exception("in MB_table_bool_4 not correct short value " + inputValue + ", need in range [0,15]");
                }
                int needLength = 4;
                var res = ((string)MB_table_bool(value)).PadLeft(needLength, '0');
                
                List<string> ar = new List<string>();
                return res.ToCharArray().Select(x => x == '0' ? "false" : "true").ToArray();
            }
        }

        public static object MB_bool(object inputValue, bool backward = false)
        {
            if (backward)
            {
                bool res = true;
                foreach (var value in (List<object>)inputValue)
                {
                    res = (bool)value && res ? true : false;
                }
                return res;
            }
            else
            {
                bool value;
                if (!Boolean.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in MB_bool not correct bool value " + inputValue);
                }

                return value;
            }
        }

        public static object MB_bool_2(object inputValue, bool backward = false)
        {
            if (backward)
            {
                return MB_bool(inputValue, backward);
            }
            else
            {
                bool value = (bool)MB_bool(inputValue, backward);
                int needLength = 2;

                List<string> ar = new List<string>();
                for (var i = 0; i < needLength; i++)
                {
                    ar.Add(value ? "true" : "false");
                }
                return ar;
            }
        }

        public static object MB_many_read(object inputValue, bool backward = false)
        {
            if (backward)
            {
                return inputValue;
            }
            else
            {
                throw new Exception("MB_many_read can't write!");
            }
        }

        public static object Volts(object inputValue, bool backward = false)
        {
            float value;
            if (!Single.TryParse(inputValue.ToString(), out value))
            {
                throw new Exception("in VoltsToAmp not correct float value " + inputValue);
            }
            return value;
        }

        public static object BoolNI(object inputValue, bool backward = false)
        {
            bool value;
            if (!Boolean.TryParse(inputValue.ToString(), out value))
            {
                throw new Exception("in BoolNI not correct bool value " + inputValue);
            }
            return value;
        }

        public static object UintNI(object inputValue, bool backward = false)
        {
            uint value;
            if (!UInt32.TryParse(inputValue.ToString(), out value))
            {
                throw new Exception("in FloatNI not correct uint value " + inputValue);
            }
            return value;
        }

        public static ushort[] RegistersToBits(ushort[] registers)
        {
            byte[] bytes = new byte[registers.Length * 2];
            int place = (registers.Length - 1) * 2;
            for (int i = 0; i < registers.Length; i++)
            {
                var r = BitConverter.GetBytes(registers[i]);
                BitConverter.GetBytes(registers[i]).CopyTo(bytes, place);
                place -= 2;
            }
            var bits = new BitArray(bytes);
            var result = new ushort[bits.Count];
            for (int i = 0; i < bits.Length; i++)
            {
                result[i] = bits[i] ? (ushort)1 : (ushort)0;
            }
            return result;
        }

        public static ushort[] BitsToRegisters(object[] bits)
        {
            bool[] boolAr = bits.Select(bit => (int)bit > 0 ? true : false).ToArray();
            Array.Reverse(boolAr);
            BitArray bitArray = new BitArray(boolAr);
            
            
            byte[] bytes = new byte[bitArray.Length / 8];
            bitArray.CopyTo(bytes, 0);

            //var registers = new ushort[1];
            //registers[0] = BitConverter.ToUInt16(bytes, 0);

            //int[] array = new int[1];
            //bitArray.CopyTo(array, 0);
            //Console.WriteLine("{0}", array[0]);

            var registers = new ushort[bytes.Length / 2];
            int place = bytes.Length - 2;
            for (int i = 0; i < registers.Length; i++)
            {
                registers[i] = BitConverter.ToUInt16(bytes, place);
                place -= 2;
            }
            return registers;
        }

        public static object PHV(object inputValue, bool backward = false)
        {
            if (backward)
            {
                bool value;
                if (!Boolean.TryParse(inputValue.ToString(), out value))
                {
                    throw new Exception("in PHV not correct bool value " + inputValue);
                }
                int newValue = value ? 1148 : 1084;
                return Converters.RegistersToInt16(newValue, backward);
            }
            else
            {
                int res = (short)Converters.RegistersToInt16(inputValue, backward);
                return res > 1148 ? true : false;
            }
        }
    }
}
