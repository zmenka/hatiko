﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Configuration;

namespace hatiko.ServiceInterface
{
    public class ConfigClassList
    {
        public ConfigClass list { get; set; }
    }

    public class ConfigClass
    {
        public string code { get; set; }
        public string device_address { get; set; }
        public string type_func { get; set; }
        public string device_type { get; set; }
        public string source { get; set; }
        public string async { get; set; }
        public string type { get; set; }
    }

    public class ConfigSingleton
    {
        private static ConfigSingleton instance;
        private List<ConfigClass> config;

        private ConfigSingleton() 
        {
            Console.WriteLine("init ConfigSingleton");
            update();
        }

        public void update() 
        {
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                var appSettings1 = new AppSettings();
                string url = appSettings1.GetRequiredString("ConfigUrl");

                var a = client.DownloadString(url);

                var ser = new System.Web.Script.Serialization.JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
                config = ser.Deserialize<List<ConfigClass>>(a);

                Console.WriteLine("update ConfigSingleton");
            }
        }

        public List<ConfigClass> getAll()
        {
            return config;
        }

        public List<string> getListByUid(string uid)
        {
            if (String.IsNullOrEmpty(uid))
            {
                throw new Exception(string.Format("Task with uid {0} has wrong config. Go to admin. ", uid));
            }

            if (config == null)
            {
                throw new Exception("Config is empty. Go to admin. ");
            }

            return config.Where(item => item.code == "uid=" + uid)
                .Select(item => new List<string>(){
                    item.device_type, 
                    item.device_address, 
                    item.source, 
                    item.type_func,
                    item.type,
                    item.async
                }).First();
        }

        public static ConfigSingleton Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new ConfigSingleton();
                }
                return instance;
            }
        }
    }
}
