﻿using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modbus.Device;

namespace hatiko.Modbus
{
    public class ModbusRead : IMbTask<ushort[]>
    {
        public byte slaveId { get; set; }
        public ushort startAddress { get; set; }
        public ushort numRegisters { get; set; }

        public ModbusRead(string deviceAddress, string source)
        {
            byte slaveId;
            if (!Byte.TryParse(deviceAddress, out slaveId))
            {
                throw new Exception("not correct slaveId " + deviceAddress);
            }
            string [] a = source.Split(':');
            if (a.Length != 2)
            {
                throw new Exception("not correct format of source " + source);
            }

            ushort startAddress;
            if (!UInt16.TryParse(a[0], out startAddress))
            {
                throw new Exception("not correct startAddress " + a[0]);
            }

            ushort numRegisters;
            if (!UInt16.TryParse(a[1], out numRegisters))
            {
                throw new Exception("not correct numRegisters " + a[1]);
            }

            this.slaveId = slaveId;
            this.startAddress = startAddress;
            this.numRegisters = numRegisters;
        }

        public ushort[] Do(IModbusSerialMaster master)
        {
            ushort[] registers = master.ReadHoldingRegisters(slaveId, startAddress, numRegisters);

            return registers;
        } 
    }

    public class ModbusWrite : IMbTask<object>
    {
        public byte slaveId { get; set; }
        public ushort startAddress { get; set; }
        public ushort[] registers { get; set; }

        public ModbusWrite(string deviceAddress, string source, object registers)
        {
            byte slaveId;
            if (!Byte.TryParse(deviceAddress, out slaveId))
            {
                throw new Exception("not correct slaveId " + deviceAddress);
            }
            string[] a = source.Split(':');
            if (a.Length != 1 && a.Length != 2)
            {
                throw new Exception("not correct format of source " + source);
            }

            ushort startAddress;
            if (!UInt16.TryParse(a[0], out startAddress))
            {
                throw new Exception("not correct startAddress " + a[0]);
            }

            this.slaveId = slaveId;
            this.startAddress = startAddress;
            this.registers = (ushort[])registers;
        }

        public object Do(IModbusSerialMaster master)
        {
            master.WriteMultipleRegisters(slaveId, startAddress, registers);

            return null;
        }
    }
}
