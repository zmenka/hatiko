﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Configuration;
using ServiceStack;
using System.IO.Ports;
using Modbus.Device;

namespace hatiko.Modbus
{
    public class ModbusClass
    {
        //public List<T> Do<T>(byte address, ITask<T>[] tasks)
        //{
        //    if (tasks == null || tasks.Count() == 0)
        //    {
        //        throw new Exception("No tasks for Modbus");
        //    }
            
        //    var appSettings = new AppSettings();
        //    var comPort = appSettings.GetRequiredString("ModbusComPort");
        //    var schemaAddress = address;

        //    ModbusClient modbusClient = new ModbusClient(comPort);
        //    modbusClient.UnitIdentifier = schemaAddress;
        //    modbusClient.Connect();

        //    var listOfResults = new List<T>();
        //    foreach (var task in tasks)
        //    {
        //        listOfResults.Add(task.Do(modbusClient));
        //    }
        //    modbusClient.Disconnect();

        //    return listOfResults;
        //}

        public T DoOne<T>(IMbTask<T> task, int attempt = 0)
        {

            var appSettings = new AppSettings();
            var comPort = appSettings.GetRequiredString("ModbusComPort");
            var baudRate = appSettings.Get<int>("ModbusBaudRate");
            var dataBits = appSettings.Get<int>("ModbusDataBits");
            var parity = appSettings.Get<int>("ModbusParity");
            var stopBits = appSettings.Get<int>("ModbusStopBits");

            try
            {
                using (SerialPort port = new SerialPort(comPort))
                {
                    // configure serial port
                    port.BaudRate = baudRate;
                    port.DataBits = dataBits;
                    port.Parity = (Parity)parity;
                    port.StopBits = (StopBits)stopBits;
                    port.Open();

                    // create modbus master
                    IModbusSerialMaster master = ModbusSerialMaster.CreateRtu(port);
                    return task.Do(master);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                ++attempt;
                if (attempt < 10)
                {
                    //возможно порт занят другой задачей
                    System.Threading.Thread.Sleep(200);
                    Console.WriteLine("COM port busy!! We are sleeping. {0}", ex.Message);
                    return DoOne(task, attempt);
                }
                else
                {
                    throw ex;
                }
                
            }
        }
    }
}
