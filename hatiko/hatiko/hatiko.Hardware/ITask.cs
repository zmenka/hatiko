﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modbus.Device;

namespace hatiko
{
    public interface ITask<T>
    {
        T Do();
    }

    public interface IMbTask<T>
    {
        T Do(IModbusSerialMaster master);
    }

    public delegate object IFunc(object parameters, bool backward = false);
}
