﻿using System;
using NUnit.Framework;
using hatiko;
using hatiko.ServiceInterface;
using hatiko.ServiceModel;
using ServiceStack.Testing;
using ServiceStack;
using ServiceStack.Configuration;

namespace hatiko.Tests
{
    [TestFixture]
    public class TaskTest
    {
        private readonly ServiceStackHost appHost;
        private AppSettings appSettings = new AppSettings();

        public TaskTest()
        {
            appHost = new AppHost()
               .Init()
               .Start(appSettings.GetRequiredString("DebugListeningOn"));
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            appHost.Dispose();
        }

        [Test]
        public void TaskSimple()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var response = client.Send(new TaskRequest { uid = "1" });
            Console.WriteLine(response.Data);
            Console.WriteLine(response.Success);

            Assert.That(response.Success, Is.EqualTo(true));
        }
    }
}
