﻿using System;
using NUnit.Framework;
using hatiko;
using hatiko.ServiceInterface;
using hatiko.ServiceModel;
using ServiceStack.Testing;
using ServiceStack;
using ServiceStack.Configuration;

namespace hatiko.Tests
{
    [TestFixture]
    public class ModbusNITest
    {
        private readonly ServiceStackHost appHost;
        private AppSettings appSettings = new AppSettings();

        public ModbusNITest()
        {
            appHost = new AppHost()
               .Init()
               .Start(appSettings.GetRequiredString("DebugListeningOn"));
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            appHost.Dispose();
        }

        [Test]
        public void MbReadFloat()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var response = client.Get(new TaskRequest { uid = "1" });
            Console.WriteLine(response.Data);
            Console.WriteLine(response.Success);

            Assert.That(response.Success, Is.EqualTo(true));
        }



        [Test]
        public void MbWriteInt()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var response = client.Get(new TaskRequest { uid = "2", write = "1000" });
            Console.WriteLine(response.Data);
            Console.WriteLine(response.Success);

            Assert.That(response.Success, Is.EqualTo(true));
        }

        [Test]
        public void MbWriteBool()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));
            //[0,0,0,0,0,0,1,1,1,1,1,0,1,0,0,0]                                                                                                                
            var response = client.Get(new TaskRequest { uid = "2", write = "1000" });
            Console.WriteLine(response.Data);
            Console.WriteLine(response.Success);

            Assert.That(response.Success, Is.EqualTo(true));
        }

        [Test]
        public void NiReadOne()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var response = client.Get(new TaskRequest { uid = "4" });
            Console.WriteLine(response.Data);
            Console.WriteLine(response.Success);

            Assert.That(response.Success, Is.EqualTo(true));
        }

        [Test]
        public void NiReadMany()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var response = client.Get(new TaskRequest { uid = "5" });
            Console.WriteLine(response.Data);
            Console.WriteLine(response.Success);

            Assert.That(response.Success, Is.EqualTo(true));
        }
    }
}
