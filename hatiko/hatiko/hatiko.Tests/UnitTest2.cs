﻿using System;
using NUnit.Framework;
using hatiko;
using hatiko.ServiceInterface;
using hatiko.ServiceModel;
using ServiceStack.Testing;
using ServiceStack;
using ServiceStack.Configuration;
using System.IO;

namespace hatiko.Tests
{
    [TestFixture]
    public class UnitTests2
    {
        private readonly ServiceStackHost appHost;
        private AppSettings appSettings = new AppSettings();

        public UnitTests2()
        {
            appHost = new AppHost()
               .Init()
               .Start(appSettings.GetRequiredString("DebugListeningOn"));
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            appHost.Dispose();
        }

        [Test]
        public void SimpleHello()
        {
            Console.WriteLine(appSettings.GetRequiredString("DebugListeningOn"));
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var response = client.Get(new Hello { Name = "World" });
            Console.WriteLine(response.Result);

            Assert.That(response.Result, Is.EqualTo("Hello, World!"));
 
        }


        [Test]
        public void EncryptedHello()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var path = appSettings.GetRequiredString("RsaPrivateKeyPath");
            if (!File.Exists(path))
            {
                throw new Exception("Wrong path: " + path);
            }
            var rsaPrivateKey = File.ReadAllText(path);

            IEncryptedClient encryptedClient = client.GetEncryptedClient(rsaPrivateKey);

            var response = encryptedClient.Send(new Hello { Name = "World" });
            Console.WriteLine(response.Result);

            Assert.That(response.Result, Is.EqualTo("Hello, World!"));
        }

        [Test]
        public void CanSendEncryptedMessage()
        {
            var client = new JsonServiceClient(appSettings.GetRequiredString("DebugListeningOn"));

            var request = new Hello { Name = "World" };

            var path = appSettings.GetRequiredString("RsaPublicKeyPath");
            if (!File.Exists(path))
            {
                throw new Exception("Wrong path: " + path);
            }
            var rsaPuplicKey = File.ReadAllText(path);

            var encRequest = RsaUtils.Encrypt(request.ToJson(), rsaPuplicKey);
            Console.WriteLine(encRequest);
            var encResponse = client.Post(new BasicEncryptedMessage
            {
                OperationName = typeof(Hello).Name,
                EncryptedBody = encRequest
            });


            path = appSettings.GetRequiredString("RsaPrivateKeyPath");
            if (!File.Exists(path))
            {
                throw new Exception("Wrong path: " + path);
            }
            var rsaPrivateKey = File.ReadAllText(path);

            var responseJson = RsaUtils.Decrypt(encResponse.EncryptedBody, rsaPrivateKey);
            var response = responseJson.FromJson<HelloResponse>();
            Console.WriteLine(response.Result);
            Assert.That(response.Result, Is.EqualTo("Hello, World!"));
        }
    }
}
