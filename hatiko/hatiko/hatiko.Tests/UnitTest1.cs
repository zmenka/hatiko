﻿using System;
using NUnit.Framework;
using hatiko;
using hatiko.ServiceInterface;
using hatiko.ServiceModel;
using ServiceStack.Testing;
using ServiceStack;

namespace hatiko.Tests
{
    [TestFixture]
    public class UnitTests
    {
        private readonly ServiceStackHost appHost;

        public UnitTests()
        {
            appHost = new BasicAppHost(typeof(MyServices).Assembly)
            {
                ConfigureContainer = container =>
                {
                    //Add your IoC dependencies here
                }
            }
            .Init();
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            appHost.Dispose();
        }

        [Test]
        public void HelloGood()
        {
            var service = appHost.Container.Resolve<MyServices>();

            var response = (HelloResponse)service.Get(new Hello { Name = "World" });

            Assert.That(response.Result, Is.EqualTo("Hello, World!"));
        }

        [Test]
        public void HelloBad()
        {
            var service = appHost.Container.Resolve<MyServices>();

            try
            {
                var response = (HelloResponse)service.Get(new Hello { Name = "" });
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("err: {0}", ex);
            }            
        }

        //[Test]
        //public void GenRsa()
        //{
        //    var rsaKeyPair = RsaUtils.CreatePublicAndPrivateKeyPair();
        //    Console.WriteLine("1");
        //    Console.WriteLine(rsaKeyPair.PrivateKey);
        //    Console.WriteLine("2");
        //    Console.WriteLine(rsaKeyPair.PublicKey);

        //}

    }
}
