﻿using Funq;
using ServiceStack;
using hatiko.ServiceInterface;
using ServiceStack.Configuration;
using System.IO;
using System;
using System.Security.Cryptography;
using hatiko.ServiceModel;
using ServiceStack.Text;

namespace hatiko
{
    public class AppHost : AppHostHttpListenerBase
    {
        /// <summary>
        /// Default constructor.
        /// Base constructor requires a name and assembly to locate web service classes. 
        /// </summary>
        public AppHost()
            : base("hatiko", typeof(MyServices).Assembly)
        {

        }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        /// <param name="container"></param>
        public override void Configure(Container container)
        {
            //var appSettings = new AppSettings();
            //var path = appSettings.GetRequiredString("RsaPrivateKeyPath");
            //if (!File.Exists(path))
            //{
            //    throw new Exception("Wrong path: " + path);
            //}
            //var rsaPrivateKey = File.ReadAllText(path);
            //Console.WriteLine("RSA length: {0}", rsaPrivateKey);
            //Plugins.Add(new EncryptedMessagesFeature
            //{
            //    PrivateKeyXml = rsaPrivateKey
            //});

            //var rsaPrivateKey = AppConfig.GetFile("RsaPrivateKeyPath");
            //var rsaPublicKey = AppConfig.GetFile("RsaPublicKeyPath");

            //RequestConverters.Add((req, requestDto) =>
            //{
            //    var encRequest = requestDto as BasicEncryptedMessage;
            //    if (encRequest == null)
            //        return null;

            //    var requestType = Metadata.GetOperationType(encRequest.OperationName);
            //    try
            //    {
            //        var decryptedJson = RsaUtils.Decrypt(encRequest.EncryptedBody, rsaPrivateKey);
            //        var request = JsonSerializer.DeserializeFromString(decryptedJson, requestType);

            //        req.Items["_encrypt"] = encRequest;
            //        return request;

            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.ToString());
            //        return null;
            //    }

            //});

            //ResponseConverters.Add((req, response) =>
            //{
            //    if (!req.Items.ContainsKey("_encrypt"))
            //        return null;

            //    var encResponse = RsaUtils.Encrypt(response.ToJson(), rsaPublicKey);
            //    return new BasicEncryptedMessageResponse
            //    {
            //        OperationName = response.GetType().Name,
            //        EncryptedBody = encResponse
            //    };
            //});
 
        }
    }
}
