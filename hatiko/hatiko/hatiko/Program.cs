﻿using System;
using System.ServiceProcess;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;
using ServiceStack.Configuration;
using System.Collections;

namespace hatiko
{
    class Program
    {
        static void Main(string[] args)
        {
            var appHost = new AppHost();
            var appSettings = new AppSettings();
            //Allow you to debug your Windows Service while you're deleloping it. 
#if DEBUG
           
            Console.WriteLine("Running WinServiceAppHost in Console mode");
            try
            {
                string listeningOn = appSettings.GetRequiredString("DebugListeningOn");

                appHost.Init();
                appHost.Start(listeningOn);
                //Process.Start(listeningOn);
                Console.WriteLine("listeningOn: {0}", listeningOn);
                Console.WriteLine("Press <CTRL>+C to stop.");
                Thread.Sleep(Timeout.Infinite);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: {0}: {1}", ex.GetType().Name, ex.Message);
                throw;
            }
            finally
            {
                appHost.Stop();
            }

            Console.WriteLine("WinServiceAppHost has finished");

#else
            //When in RELEASE mode it will run as a Windows Service with the code below

            string listeningOn = appSettings.GetRequiredString("ListeningOn");
                
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new WinService(appHost, listeningOn) 
			};
            ServiceBase.Run(ServicesToRun);

#endif

            Console.ReadLine();
        }
    }
}
