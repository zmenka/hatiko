﻿using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;
using hatiko.Modbus;
using hatiko.NI.AnalogIn;
using hatiko.NI.Digital;
using hatiko.ServiceModel;

namespace hatiko.ServiceInterface
{
    class Task
    {
        public object result;

        public string uid;

        public string write;
        public int delay;
        public int n;
        public int nafter;

        private TaskConfig config;

        public Task(string uid, string write, int n, int delay, int nafter)
        {
            this.uid = uid;
            this.write = write;
            this.n = n;
            this.delay = delay;
            this.nafter = nafter;

            this.config = new TaskConfig(uid);
        }

        public async System.Threading.Tasks.Task<bool> Do()
        {
            try
            {
                if (this.delay > 0)
                {
                    //System.TimeSpan ts = new TimeSpan(0,0,0,0, this.delay);
                    //System.Threading.Thread.Sleep(this.delay);
                    await System.Threading.Tasks.Task.Delay(this.delay);
                }
                if (this.config.DeviceType == DeviceTypeEnum.Modbus)
                {
                    
                    var mb = new ModbusClass();
                    if (this.write != null)
                    {
                        if (this.n > 1)
                        {
                            object[] resAr = new object[n];
                            for (int i = 0; i < n; i++)
                            {
                                var registers = this.config.TypeFunc(this.write, true);
                                var task = new ModbusWrite(config.DeviceAddress, config.Source, registers);
                                resAr[i] = mb.DoOne(task);

                                if (this.delay > 0)
                                {
                                    System.TimeSpan ts = new TimeSpan(0, 0, 0, 0, this.delay);
                                    System.Threading.Thread.Sleep(this.delay);
                                }
                            }
                            this.result = resAr;
                        }
                        else
                        {
                            var registers = this.config.TypeFunc(this.write, true);
                            var task = new ModbusWrite(config.DeviceAddress, config.Source, registers);
                            this.result = mb.DoOne(task);
                        }
                    }
                    else
                    {
                        if (this.n > 1)
                        {
                            object[] resAr = new object[n];
                            for (int i = 0; i < n; i++)
                            {
                                var task = new ModbusRead(config.DeviceAddress, config.Source);
                                var res = mb.DoOne(task);
                                resAr[i] = this.config.TypeFunc(res);

                                if (this.delay > 0)
                                {
                                    System.TimeSpan ts = new TimeSpan(0, 0, 0, 0, this.delay);
                                    System.Threading.Thread.Sleep(this.delay);
                                }
                            }
                            this.result = resAr;
                        }
                        else
                        {
                            var task = new ModbusRead(config.DeviceAddress, config.Source);
                            var res = mb.DoOne(task);
                            this.result = this.config.TypeFunc(res);
                        }
                    }
                }
                else if (this.config.DeviceType == DeviceTypeEnum.NIAnalog)
                {
                    if (this.write != null)
                    {
                        throw new Exception("we dont process writeing to NI Analog");
                    }
                    else
                    {

                        if (this.n > 1)
                        {
                            if (string.IsNullOrEmpty(config.AsyncInfo))
                            {
                                var voltageMany = new VoltageRange();
                                var res = voltageMany.Start(config.DeviceAddress, config.Source, this.n);
                                this.result = res.Select(x => this.config.TypeFunc(x));
                            } else {
                                var voltageMany = new VoltageRangeWithTrigger();
                                var res = voltageMany.Start(config.DeviceAddress, config.Source, this.n, this.nafter, config.AsyncInfo);

                                this.result = res.Select(x => this.config.TypeFunc(x));
                            }
                            
                        }
                        else
                        {
                            var voltageOne = new VoltageOne();
                            var res = voltageOne.Start(config.DeviceAddress, config.Source);
                            this.result = this.config.TypeFunc(res);
                        }
                        
                    }
                } 
                else if (this.config.DeviceType == DeviceTypeEnum.NIDigital)
                {
                    if (this.write != null)
                    {
                        var wc = new WriteChannel();
                        var value = this.config.TypeFunc(this.write);
                        wc.Start(config.DeviceAddress, value);
                    }
                    else
                    {

                        var rc = new ReadChannel();
                        var res = rc.Start(config.DeviceAddress);
                        this.result = res;

                    }
                }
                else if (this.config.DeviceType == DeviceTypeEnum.Virtual)
                {
                    string[] tasksUid = this.config.DeviceAddress.Split(':');

                    if (this.write != null)
                    {
                        object[] values = (object[])this.config.TypeFunc(this.write);
                        if (tasksUid.Length != values.Length)
                        {
                            throw new Exception("virtual task: uid Length != values length");
                        }
                        var taskList = new List<Task>();
                        for (var i = 0; i < tasksUid.Length; i++)
                        {
                            var tt = new Task(tasksUid[i].ToString(), values[i].ToString(), 0, 0, 0);
                            taskList.Add(tt);
                        }
                        var t = new Tasks(taskList);

                        var resBool = await t.Do(false);

                        if (resBool)
                        {
                            this.result = this.write;
                        }
                        else
                        {
                            this.result = t.result;
                        }
                        return resBool;
                    }
                    else
                    {
                        var taskList = new List<Task>();
                        for (var i = 0; i < tasksUid.Length; i++)
                        {
                            var tt = new Task(tasksUid[i].ToString(), null, 0, 0, 0);
                            taskList.Add(tt);
                        }
                        var t = new Tasks(taskList);

                        var resBool = await t.Do(false);
                        if (resBool)
                        {
                            this.result = this.config.TypeFunc(t.result, true);
                        }
                        else
                        {
                            this.result = t.result;
                        }
                        
                        return resBool;
                    }
                    
                }
                else
                {
                    throw new Exception("we dont process this type device");
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                this.result = e.ToString();
                return false;
            }
        }

    }

    class Tasks
    {
        public object result;

        public readonly List<Task> tasks;

        public Tasks(List<Task> tasks)
        {
            this.tasks = tasks;
        }

        public async System.Threading.Tasks.Task<bool> Do(bool parallel = true)
        {
            try
            {
                List<object> result = new List<object>();
                Console.WriteLine(DateTime.Now);
                if (parallel)
                {
                    var asyncTasks = tasks.Select(task =>task.Do()).ToArray();
                    await System.Threading.Tasks.Task.WhenAll(asyncTasks);
                    for (var i=0; i<asyncTasks.Length; i++)
                    {
                        if (!asyncTasks[i].Result)
                        {
                            throw new Exception("uid " + tasks[i].uid + ", " + tasks[i].result.ToString());
                        }
                        result.Add(tasks[i].result);
                    }
                }
                else
                {
                    foreach (var task in tasks)
                    {
                        var res = await task.Do();
                        if (!res)
                        {
                            throw new Exception("uid " + task.uid + ", " + task.result.ToString());
                        }
                        result.Add(task.result);
                        //System.Threading.Thread.Sleep(200);
                    }
                }
                
                this.result = result;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                this.result = e.ToString();
                return false;
            }
        }
    }
}
