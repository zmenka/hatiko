﻿using ServiceStack.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hatiko.ServiceInterface
{
    public static class Logger
    {
        public static void WriteToFile(string text)
        {
            Console.WriteLine(text);
            var appSettings = new AppSettings();
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, appSettings.GetRequiredString("LogFileName"));
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine("date: {0}, {1}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"), text);
                writer.Close();
            }
        }
    }
}
