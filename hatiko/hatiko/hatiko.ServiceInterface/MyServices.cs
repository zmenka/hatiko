﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack;
using hatiko.ServiceModel;
using System.Web.Script.Serialization;
using System.Reflection;
using hatiko.Modbus;
using System.Globalization;
using System.Xml.Serialization;
using System.IO;

namespace hatiko.ServiceInterface
{

    public class MyServices : Service
    {
        public object Get(Hello request)
        {
            if (string.IsNullOrEmpty(request.Name))
                throw new ArgumentNullException("Name");
            var m = new ModbusClass();
            this.Response.AddHeader("Access-Control-Allow-Origin", "*");
            return new HelloResponse { Result = "Hello2, {0}!".Fmt(request.Name) }; ;
        }

        public object Get(UpdateConfig request)
        {
            ConfigSingleton.Instance.update();
            this.Response.AddHeader("Access-Control-Allow-Origin", "*");
            return new GeneralResponse
                   {
                       Success = true,
                       Data = ConfigSingleton.Instance.getAll()
                   };
        }

        public object Any(BasicEncryptedMessage request)
        {
            if (string.IsNullOrEmpty(request.OperationName))
                throw new ArgumentNullException("OperationName");
            return new HelloResponse { Result = "Hello, {0}!".Fmt(request.OperationName) };
        }

        public async System.Threading.Tasks.Task<object> Get(TaskRequest request)
        {
            this.Response.AddHeader("Access-Control-Allow-Origin", "*");
            Task t;
            try
            {
                if (string.IsNullOrEmpty(request.uid))
                {
                    throw new Exception("task with null uid ");
                }

                int n = 0;
                if (request.n != null && !Int32.TryParse(request.n, out n))
                {
                    throw new Exception(string.Format("task with uid {0} has wrong param n: {1}", request.uid, request.n));
                }

                if (n > 500 || n < 0)
                {
                    throw new Exception(string.Format("task with uid {0} has wrong param n: {1}, not in range [0,500]", request.uid, request.n));
                }

                int nafter = 0;
                if (request.nafter != null && !Int32.TryParse(request.nafter, out nafter))
                {
                    throw new Exception(string.Format("task with uid {0} has wrong param nafter: {1}", request.uid, request.nafter));
                }

                if (nafter > 500 || nafter < 0)
                {
                    throw new Exception(string.Format("task with uid {0} has wrong param nafter: {1}, not in range [0,500]", request.uid, request.nafter));
                }

                int delay = 0;
                if (request.delay != null && !Int32.TryParse(request.delay, out delay))
                {
                    throw new Exception(string.Format("task with uid {0} has wrong param delay: {1}", request.uid, request.delay));
                }
                //float delay = 0;
                //NumberFormatInfo DecimalSeparatorFormat = new NumberFormatInfo { NumberDecimalSeparator = "."};
                //if (request.delay != null && !Single.TryParse(request.delay, NumberStyles.Float, DecimalSeparatorFormat, out delay))
                //{
                //    throw new Exception(string.Format("task with uid {0} has wrong param delay: {1}", request.uid, request.delay));
                //}
                t = new Task(request.uid, request.write, n, delay, nafter);  
                var res = await t.Do();

                if (!res)
                {
                    Logger.WriteToFile("task " + request.ToString() + ": " + t.result.ToString());
                }
                return this.Request.ToOptimizedResult(new GeneralResponse
                {
                    Success = res,
                    Data = t.result
                });
            }
            catch (Exception e)
            {
                Logger.WriteToFile(e.ToString());
                return this.Request.ToOptimizedResult(
                   new GeneralResponse
                   {
                       Success = false,
                       Data = e.ToString()
                   });
            }

        }


        public async System.Threading.Tasks.Task<object> Get(TaskManyRequest request)
        {
            this.Response.AddHeader("Access-Control-Allow-Origin", "*");
            Tasks t; 
            try
            {
                var tasks = new List<Task>();

                foreach (var task in request.tasks)
                {
                    if (task.uid == null)
                    {
                        throw new Exception("task with null uid ");
                    }

                    int n=0;
                    if (task.n != null && !Int32.TryParse(task.n, out n))
                    {
                        throw new Exception(string.Format("task with uid {0} has wrong param n: {1}", task.uid, task.n));
                    }

                    if (n > 500 || n < 0)
                    {
                        throw new Exception(string.Format("task with uid {0} has wrong param n: {1}, not in range [0,500]", task.uid, task.n));
                    }

                    int nafter = 0;
                    if (task.nafter != null && !Int32.TryParse(task.nafter, out nafter))
                    {
                        throw new Exception(string.Format("task with uid {0} has wrong param nafter: {1}", task.uid, task.nafter));
                    }

                    if (nafter > 500 || nafter < 0)
                    {
                        throw new Exception(string.Format("task with uid {0} has wrong param nafter: {1}, not in range [0,500]", task.uid, task.nafter));
                    }

                    int delay = 0;
                    if (task.delay != null && !Int32.TryParse(task.delay, out delay))
                    {
                        throw new Exception(string.Format("task with uid {0} has wrong param delay: {1}", task.uid, task.delay));
                    }
                    //float delay = 0;
                    //NumberFormatInfo DecimalSeparatorFormat = new NumberFormatInfo { NumberDecimalSeparator = "." };
                    //if (task.delay != null && !Single.TryParse(task.delay, NumberStyles.Float, DecimalSeparatorFormat, out delay))
                    //{
                    //    throw new Exception(string.Format("task with uid {0} has wrong param delay: {1}", task.uid, task.delay));
                    //}
                    var tt = new Task(task.uid, task.write, n, delay, nafter);
                    tasks.Add(tt);

                }

                t = new Tasks(tasks);
                
                var res = await t.Do();

                if (!res)
                {
                    Logger.WriteToFile("tasks " + request.ToString() + ": " + t.result.ToString());
                }  
                return this.Request.ToOptimizedResult(
                    new GeneralResponse
                    {
                        Success = res,
                        Data = t.result
                    });
            }
            catch (Exception e)
            {
                Logger.WriteToFile(e.ToString());
                return this.Request.ToOptimizedResult(
                   new GeneralResponse
                   {
                       Success = false,
                       Data = e.ToString()
                   });
            }

        }

        public string ToJsonString<T>(T toSerialize)
        {
            return new JavaScriptSerializer().Serialize(toSerialize);
        }
    }
}